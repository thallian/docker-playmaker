FROM registry.gitlab.com/thallian/docker-confd-env:debian

ENV VERSION=v0.6.2
ENV LANG_LOCALE=en_US.UTF-8
ENV ANDROID_HOME=/opt/android-sdk-linux
ENV PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
ENV DEBIAN_FRONTEND=noninteracative

RUN useradd -d /home/playmaker -m -u 2222 -U playmaker

RUN mkdir -p /usr/share/man/man1
RUN apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip \
    lib32stdc++6 \
    lib32gcc1 \
    lib32z1 \
    lib32ncurses5 \
    libffi-dev \
    libssl-dev \
    libjpeg-dev \
    libxml2-dev \
    libxslt1-dev \
    openjdk-8-jdk-headless \
    wget \
    unzip \
    zlib1g-dev

RUN mkdir -p $ANDROID_HOME

RUN wget https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip && \
    echo "444e22ce8ca0f67353bda4b85175ed3731cae3ffa695ca18119cbacef1c1bea0  sdk-tools-linux-3859397.zip" | sha256sum -c && \
    unzip sdk-tools-linux-3859397.zip && \
    rm sdk-tools-linux-3859397.zip && \
    wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip && \
    unzip platform-tools-latest-linux.zip && \
    mv platform-tools $ANDROID_HOME && \
    echo 'y' | tools/bin/sdkmanager --sdk_root=/opt/android-sdk-linux --verbose "platforms;android-26" &&\
    tools/bin/sdkmanager --sdk_root=/opt/android-sdk-linux --verbose "build-tools;26.0.1" && \
    mv tools $ANDROID_HOME && \
    mkdir -p /opt/playmaker && \
    wget -qO- https://github.com/NoMore201/playmaker/archive/$VERSION.tar.gz | tar xz -C /opt/playmaker --strip 1 && \
    cd /opt/playmaker && pip3 install . && \
    cd /opt && rm -rf playmaker

RUN pip3 install fdroidserver
RUN pip3 install --upgrade google-auth-oauthlib

RUN cp /usr/local/lib/python3.5/dist-packages/usr/share/doc/fdroidserver/examples/fdroid-icon.png /home/playmaker

RUN /var/lib/dpkg/info/ca-certificates-java.postinst configure

RUN mkdir /home/playmaker/keys/

RUN chown -R playmaker:playmaker /home/playmaker

WORKDIR /home/playmaker

ADD /rootfs /

VOLUME ["/home/playmaker/repo/", "/home/playmaker/keys/"]

EXPOSE 5000
