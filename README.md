[Playmaker](https://github.com/NoMore201/playmaker) is an Fdroid repository manager,
fetching apps from Play Store

# Volumes
- `/home/playmaker/repo`
- `/home/playmaker/keys`

# Environment variables
## GOOGLE_ACCOUNT
Email of the google account used to download apks.

## GOOGLE_PASSWORD
Password for the google account. Use an app password.

## REPO_URL
Base domain.

## REPO_NAME
Name of the repository

## KEYSTORE_PASSWORD
Password for the keystore.

# Port
- 5000
